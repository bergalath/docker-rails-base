<?php

    function adminer_object()
    {
        // required to run any plugin
        include_once "./plugins/plugin.php";

        // autoloader
        foreach (glob("plugins/*.php") as $filename) {
            include_once "./$filename";
        }

        $re = '/(\w+):\/\/(\w+):([[:alnum:]]+)@([\w\.]+):(\d+)/m';
        preg_match_all($re, $_ENV["LOCAL_DB_URL"], $db_matches,  PREG_SET_ORDER, 0);
        $local_db = $db_matches[0][4];

        $plugins = array(
            new AdminerLoginServersEnhanced(
                array(
                    new AdminerLoginServerEnhanced(
                        $local_db,
                        'Local (' . $local_db . ')',
                        'pgsql'),
                )
            ),
            new AdminerDatabaseHide(["pghero", "postgres", "template0", "template1"]),
            new AdminerTablesFilter(),
        );

        class AdminerCustomization extends AdminerPlugin {
          // see: https://github.com/vrana/adminer/blob/7af1ee3702420620641d075ebfd54d4b1d220409/adminer/include/adminer.inc.php#L18-L20

            function parse_db_url($db_url) {
                $re = '/(\w+):\/\/(\w+):([[:alnum:]]+)@([\w\.]+):(\d+)/m';
                preg_match_all($re, $db_url, $matches,  PREG_SET_ORDER, 0);
                return array(SERVER, $matches[0][2], $matches[0][3]);
            }

            function credentials() {
                if (!empty($_GET['username']))  return array(SERVER, $_GET["username"], get_password());
                if ("db" == SERVER)             return $this->parse_db_url($_ENV["LOCAL_DB_URL"]);
            }

            function name() {
                return "<a href='http://app.localhost:81'" . target_blank() . " id='h1'>Alteriel’s Adminer </a>";
            }
        }

        return new AdminerCustomization($plugins);
    }

    // include original Adminer or Adminer Editor
    include "./adminer.php";

?>
