#
# Alteriel’s Makefile <technique@immateriel.fr>
#

# docker compose exec web bundle exec rails c
# docker compose run --rm --no-deps web rails new kaka_huet -d postgresql -a propshaft -j importmap -c bulma -B

# do not echo commands as we run them
.SILENT:

SHELL != which bash

new: ## Nouvelle appli
	docker compose run --rm --no-deps web rails new kaka_huet -d postgresql -a propshaft -j importmap -c bulma -B

up: ## Démarre
	docker compose up web --detach

stop: ## Arrête
	docker compose stop

restart: stop ## Redémarre
	docker compose up web --detach --remove-orphans

rebuild: stop ## Arrête, supprime, reconstruit et redémarre
	docker system prune -f
	docker compose up web --always-recreate-deps --build --detach --force-recreate --remove-orphans

launch: up ## Exécute une commande
	docker compose exec web fish

run: ## Exécute une commande dans un nouveau container
	docker compose run --rm web $(or $(args),fish)

all: up ## No-op

test: ## Exécute la suite de tests
	-docker compose run --rm -e RAILS_ENV=test web test $(args)
	# -docker compose rm -fsv selenium_chrome > /dev/null

console: up ## Démarre la console Rails
	docker compose exec web bundle exec rails console

be: up ## Exécute une commande `bundle exec` dans le container
	docker compose exec web bundle exec $(args)

b: up ## Exécute une commande `bundle` dans le container
	docker compose exec web bundle $(args)

clean: ## Nettoie les logs et fichiers temporaires
	docker compose exec web bundle exec rake log:clear tmp:clear assets:clean

%:
	@

help:
	grep -E '^\S+:.*## .*$$' $(MAKEFILE_LIST) | sed -En 's/(\w+):.*?## (.*)$$/\1|\2/p' | awk -F'|' '{printf "\033[36m%-15s\033[0m %s\n", $$1, $$2}'

.PHONY: up stop restart rebuild launch run log all test coverage console be b deploy pool set_urls
.PHONY: sync stream set_config clean version projets checks debug release login push help
.ONESHELL: all checks clean
.DEFAULT_GOAL := help
