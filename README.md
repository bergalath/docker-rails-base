# Docker with Rails 7

1. Clone repo and rename new folder to project name [$ git clone repo-name folder-name]

2. (optional) Open project in editor and rename all instances of 'myapp' to the project name.

3. Create a new rails 7 app 
```bash
docker compose run --rm --no-deps web rails new kaka_huet -d postgresql -a propshaft -j importmap -c bulma -B
```

4. Build the image
```bash
docker compose build
```

5. Replace database.yml with this (but change myapp to the project name):

```yaml
default: &default
  adapter: postgresql
  encoding: unicode
  host: db
  username: postgres
  password: password
  pool: 5

development:
  <<: *default
  database: myapp_development

test:
  <<: *default
  database: myapp_test
```

 6. Start containers
 ```bash
 docker compose up
 ```
 
 7. Create the database
 ```bash
 docker compose run web rake db:create
 ```
docker compose run --rm --no-deps web rails new kaka_huet -d postgresql -a propshaft -j importmap -c tailwind -B
docker compose run --rm --no-deps web rails new kaka_huet -d postgresql -a propshaft -j importmap -c bulma -B
bin/rails g scaffold User name email:uniq enabled:boolean password:digest
bin/rails db:migrate
bin/rails test:db
bin/rails test:all
