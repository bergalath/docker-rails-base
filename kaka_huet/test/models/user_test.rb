require "test_helper"

class UserTest < ActiveSupport::TestCase
  setup do
    @user = users(:one)
  end

  test "disabled" do
    refute @user.disabled?
    @user.toggle! :disabled
    assert @user.disabled?
  end
end
