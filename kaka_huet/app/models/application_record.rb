class ApplicationRecord < ActiveRecord::Base
  primary_abstract_class

  def self.[](column)
    arel_table[column]
  end
end

module Arel
  module Predications
    alias_method :>, :gt
    alias_method :<, :lt
    alias_method :>=, :gteq
    alias_method :<=, :lteq
    alias_method :"!=", :not_eq
    alias_method :==, :eq
  end
end
