class Player < ApplicationRecord
  has_secure_password

  validates_presence_of :name, :email
  validates_uniqueness_of :email, case_sensitive: false

  scope :enabled, -> { where enabled: true }
end
