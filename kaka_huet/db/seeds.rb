# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)

Player.destroy_all

100.times do
  name = Faker::Name.name
  player = Player.create(
    name: name,
    email: Faker::Internet.unique.email(name: name),
    enabled: rand(0..10).even?,
    password: Faker::Internet.password,
  )

  # rand(5..10).times do
  #   player.reviews.create body: Faker::Player.review
  # end
end
