class CreatePlayers < ActiveRecord::Migration[7.0]
  def change
    create_table :players do |t|
      t.string :name, null: false
      t.string :email, null: false
      t.boolean :enabled, default: true
      t.string :password_digest

      t.timestamps
    end
    add_index :players, :name
    add_index :players, :email, unique: true
  end
end
